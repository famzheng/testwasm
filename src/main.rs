use std::time::SystemTime;
use serde_json::{Result, Value};
mod data;

fn main() {
    let data = data::get_data();
    let start = SystemTime::now();

    let mut secret = None;

    for i in 1..1000 {
        let v: Value = serde_json::from_str(data).unwrap();
        secret = match &v["secret"] {
            Value::String(s) => Some(String::from(s)),
            _ => None,
        };
    }

    let dur = start.elapsed().unwrap().as_secs_f32();
    println!("{} s to find the secret is {}", dur, secret.unwrap());
}
