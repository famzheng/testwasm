all: target/x86_64-unknown-linux-gnu/release/testwasm target/wasm32-wasi/release/testwasm.wasm

target/x86_64-unknown-linux-gnu/release/testwasm: src/*.rs
	cargo build --release --target x86_64-unknown-linux-gnu

target/wasm32-wasi/release/testwasm.wasm: src/*.rs
	cargo build --release --target wasm32-wasi

run: all
	target/x86_64-unknown-linux-gnu/release/testwasm
	wasmer target/wasm32-wasi/release/testwasm.wasm
	wasmtime target/wasm32-wasi/release/testwasm.wasm
